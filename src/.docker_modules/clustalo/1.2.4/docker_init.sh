#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/clustalo:1.2.4
# docker build src/.docker_modules/clustalo/1.2.4 -t 'lbmc/clustalo:1.2.4'
# docker push lbmc/clustalo:1.2.4
docker buildx build --platform linux/amd64,linux/arm64 -t "lbmc/clustalo:1.2.4" --push src/.docker_modules/clustalo/1.2.4

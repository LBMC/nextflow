#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull mlepetit/sanity
docker build src/.docker_modules/sanity -t 'lbmc/sanity'
docker push lbmc/sanity

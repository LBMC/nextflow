#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/gatk:3.8.0
# docker build src/.docker_modules/gatk/3.8.0 -t 'lbmc/gatk:3.8.0'
# docker push lbmc/gatk:3.8.0
docker buildx build --platform linux/amd64,linux/arm64 -t "lbmc/gatk:3.8.0" --push src/.docker_modules/gatk/3.8.0

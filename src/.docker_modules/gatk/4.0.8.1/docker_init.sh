#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/gatk:4.0.8.1
# docker build src/.docker_modules/gatk/4.0.8.1 -t 'lbmc/gatk:4.0.8.1'
# docker push lbmc/gatk:4.0.8.1
docker buildx build --platform linux/amd64,linux/arm64 -t "lbmc/gatk:4.0.8.1" --push src/.docker_modules/gatk/4.0.8.1

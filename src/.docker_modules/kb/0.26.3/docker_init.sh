#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/kb:0.26.3
docker build src/.docker_modules/kb/0.26.3 -t 'lbmc/kb:0.26.3'
docker push lbmc/kb:0.26.3

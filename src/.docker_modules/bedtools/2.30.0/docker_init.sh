#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/bedtools:2.30.0
docker build src/.docker_modules/bedtools/2.30.0 -t 'lbmc/bedtools:2.30.0'
docker push lbmc/bedtools:2.30.0

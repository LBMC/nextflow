#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/htseq:1.99.2
docker build src/.docker_modules/htseq/1.99.2 -t 'lbmc/htseq:1.99.2'
docker push lbmc/htseq:1.99.2

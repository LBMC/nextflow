#!/bin/sh

# SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

docker pull lbmc/cite_seq_count:1.4.5
docker build src/.docker_modules/CITE-seq-Count/1.4.5 -t 'lbmc/cite_seq_count:1.4.5'
docker push lbmc/cite_seq_count:1.4.5
# docker buildx build --platform linux/amd64,linux/arm64 -t "lbmc/cite_seq_count:1.4.5" --push src/.docker_modules/CITE-seq-Count/1.4.5

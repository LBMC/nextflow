// SPDX-FileCopyrightText: 2022 Laurent Modolo <laurent.modolo@ens-lyon.fr>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

version = "0.11.3--pl5.22.0_0"
container_url = "quay.io/biocontainers/fastq-screen:${version}"

params.fastq_screen = ""
params.fastq_screen_out = ""
process fastq_screen {
  container = "${container_url}"
  label "big_mem_multi_cpus"
  tag "$file_id"
  if (params.index_fasta_out != "") {
    publishDir "results/${params.fastq_screen_out}", mode: 'copy'
  }

  input:
    tuple val(file_id), path(fastq)

  output:
    tuple val(file_id), path("*"), emit: output

  script:
"""
fastq_screen --get_genomes
fastq_screen --threads ${task.cpus} sample1.fastq sample2.fastq
"""
}
